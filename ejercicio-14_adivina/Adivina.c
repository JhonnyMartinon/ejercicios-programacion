#include <stdio.h>
#include <stdlib.h>

int main(){
	int Num, Res,i,vidas, Op = 1;

	while(Op != 0){
		printf("\n\nOpciones:");
		printf("\n1) Jugar.");
		printf("\n0) Salir.");
		printf("\nOpción seleccionada: ");

		scanf("%d",&Op);

		switch(Op){
			case 1:
				Num = rand() % 100 + 1;
				vidas = 5;

				printf("\n...Número generado entre 1 y 100...");
				printf("\n\t...Vidas: 5...\n");
				for (i =0;i<5;i++){
					printf("\nAdivina el número: ");
					scanf("%d",&Res);
					if(Res == Num){
						printf("\nFELICIDADES!!!!\nGANASTE!!!");
						break;
					}else{
						vidas-=1;
						printf("\n\t...Vidas: %d...\n",vidas);
					}
				}
				if(i == 5){
					printf("\nPERDISTE!!!\nEl número era: %d",Num);
				}
				break;
			case 0:
				printf("\nPrograma desarrollado por: Jonathan Martiñón.\n\n");
				break;

			default:
				printf("\n\t...ERROR; Número fuera de rango...\n");
				break;
		}
	}
}
