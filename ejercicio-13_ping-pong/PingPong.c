#include <stdio.h>

int main(){

	for(int i = 1; i<=100; i++){

		if(i%3 == 0){
			if(i%5==0){
				printf("\n%d: Ping-Pong.",i);
			}else{
				printf("\n%d: Ping.",i);
			}
		}else if (i%5==0){
			printf("\n%d: Pong.",i);
		}else{
			printf("\n%d:",i);
		}

	}
	printf("\n\n");

}
