#include <stdio.h>
#include <stdlib.h>

int main(){

	int DinU,DinC,i,ContU,ContC,ApU,ApC,Op=1,Volado,Apuesta;

	while(Op != 0){

		printf("\n\nOpciones:");
		printf("\n1) Jugar.");
		printf("\n0) Salir.");
		printf("\nOpción Seleccionada: ");

		scanf("%d",&Op);

		switch(Op){

			case 1:

				DinU = 500;
				DinC = 500;
				
				ContC = 0;
				ContU = 0;

				for (i=0;i<3;i++){
					printf("\n\n\tCPU: %d",ContC);
					printf("\n\tUSER: %d",ContU);

					ApC = rand()%81 + 20;
					
					printf("\nCPU apuesta: $%d",ApC);

					printf("\nIngresa tu monto de apuesta entre $20 y $100: $ ");
					scanf("%d",&ApU);

					if(ApU>= 20 && ApU <=100){
						printf("\n1) Águila.");
						printf("\n2) Sol.");
						printf("\nSelecciona una opción: ");
						scanf("%d",&Apuesta);

						if (Apuesta == 1 || Apuesta == 2){

							printf("\n\t...Generando volado....");
							Volado = rand() %2 + 1;

							if (Volado == 1){
								printf("\n\tSalió: Águila.");
							}else{
								printf("\n\tSalió: Sol.");
							}

							if(Volado == Apuesta){
								printf("\n\tGanaste esta ronda...");
								DinU+=ApC;
								DinC-=ApC;
								ContU+=1;
							}else{
								printf("\n\tGanó CPU esta ronda...");
								DinC+=ApU;
								DinU-=ApU;
								ContC+=1;
							}
						
						}else{
							printf("\n\t...ERROR; Apuesta no válida...\n");
							break;
						}
					}else{
						printf("\n\t...ERROR; Monto inválido...\n");
						break;
					}

					
					
				}
				if (i==3){
					if(ContU > ContC){
						printf("\n\n\tFELICIDADES!!!!\n\tGANASTE!!!!");
					}else{
						printf("\n\n\tPERDISTE!!!!!");
					}

					printf("\nPuntaje\t\tMontos");
					printf("\nCPU: %d \t\t %d",ContC,DinC);
					printf("\nUSER: %d\t\t %d",ContU,DinU);
				}

				break;
			case 0:
				printf("\n\nPrograma desarrollado por: Jonathan Martiñón.\n\n");
				break;
			default:
				printf("\n\t...ERROR; Número fuera de rango...\n");
				break;

		}
	}
}
