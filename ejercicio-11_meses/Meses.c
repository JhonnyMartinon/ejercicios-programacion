#include<stdio.h>

int main(){

	int Op = 1;
	int Mes = 0;

	while(Op!=0){

		printf("\n\nOpciones:\n");
		printf("1) Identificar mes.\n");
		printf("0) Salir.\n");
		printf("Opción seleccionada: ");
		scanf("%d",&Op);

		switch(Op){

			case 1:
				printf("\nIngresa un mes: ");
				scanf("%d",&Mes);

				switch(Mes){

					case 0:
						printf("\nEl mes es: Enero");
						break;

					case 1:
						printf("\nEl mes es: Febrero");
						break;

					case 2:
						printf("\nEl mes es: Marzo");
						break;

					case 3:
						printf("\nEl mes es: Abril");
						break;

					case 4:
						printf("\nEl mes es: Mayo");
						break;

					case 5:
						printf("\nEl mes es: Junio");
						break;

					case 6:
						printf("\nEl mes es: Julio");
						break;

					case 7:
						printf("\nEl mes es: Agosto");
						break;

					case 8:
						printf("\nEl mes es: Septiembre");
						break;

					case 9:
						printf("\nEl mes es: Octubre");
						break;

					case 10:
						printf("\nEl mes es: Noviembre");
						break;

					case 11:
						printf("\nEl mes es: Diciembre");
						break;
					default:
						printf("\n\t...ERROR; Ingresa un número entre el 0 y el 11...\n");
						break;
				}
				break;

			case 0:
				printf("Programa desarrollado por: Jonathan Martiñón.\n\n");
				break;

			default:
				printf("\n\t...ERROR; Número fuera de rango...\n");
				break;

		}

	}

}
