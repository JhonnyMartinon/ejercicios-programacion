#include <stdio.h>

// Tuve un pequeño conflicto en cuanto a los números
// Que el programa podía acpetar. En un inicio lo
// Cerré a los positivos, sin embargo, después
// Caí en cuenta que no existía ninguna restricción,
// Por lo que no importaba el número, todos podía.

int main(){

	int Op = 1;
	float Num = 0.0;

	while(Op!=0){

		printf("\n\nOpciones:\n");
		printf("1) Mostrar tabla.\n");
		printf("0) Salir.\n");
		printf("Opción Seleccionada: ");

		scanf("%d",&Op);

		switch(Op){

			case 1:
				printf("\nIngresa el número a mostrar su tabla: ");
				scanf("%f",&Num);

				for (int i = 2; i<=10; i++){
					printf("\n%.2f x %d = %.2f",Num,i,Num*i);
				}
				break;

			case 0:
				printf("\nPrograma desarrollado por: Jonathan Martiñón.\n\n");
				break;

			default:
				printf("\n\t...ERROR; Número fuera de rango...\n\n");
				break;

		}

	}

}
