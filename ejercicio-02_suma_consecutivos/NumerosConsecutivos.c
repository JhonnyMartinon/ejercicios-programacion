#include <stdio.h>

int main (){

	int Op=1;
	int Num=0;
	int Res;

	while(Op!=2){
	
		printf("\nOpciones:\n");
		printf("1) Generar nueva suma.\n");
		printf("2) Salir.\n");
		printf("Opción Seleccionada: ");
		scanf("%d",&Op);

		if (Op ==1){

			printf("Ingresa un número entre 1 y 50:  ");
			scanf("%d",&Num);

			if(Num >= 1 && Num<=50){
				Res = 1;
				if (Num != 1){
					for (int i = 2; i<=Num; i++){

						Res+=i;

					}
				}
				printf("La sumatoria desde 1 hasta %d es: %d\n",Num,Res);
			}else{
				printf("\n...ERROR; Número fuera de rango...\n\n");
			}

		}else if (Op == 2){

			printf("Programa Elaborado por: Jonathan Martiñón.\n\n");

		}else{

		printf("\n...ERROR; Número fuera de Rango...\n\n");

		}


	}

}
