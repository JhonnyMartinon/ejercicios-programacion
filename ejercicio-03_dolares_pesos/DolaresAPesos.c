#include <stdio.h>

int main(){

	double dolar = 0.0;
	double pesos = 0.0;
	int Op = 1;
	double valor = 22.18;

	while(Op!=0){

		printf("\nOpciones:\n");
		printf("1) Convertir Dólar a pesos.\n");
		printf("2) Actualizar valor de 1 dólar a pesos.\n");
		printf("0) Salir.\n");

		printf("Opción Seleccionada: ");
		scanf("%d",&Op);

		switch (Op){

			case 1:
				printf("\nValor actual del dólar: %.3f pesos.\n",valor);

				printf("Ingresa la cantidad de dólares a convertir: ");
				scanf("%lf",&dolar);

				if (dolar >= 0){

					pesos = dolar * valor;
					printf("\n%.3f dólares = %.3f pesos\n\n",dolar,pesos);

				}else{

					printf("\n\t...ERROR; Ingresa un valor positivo...\n");

				}
				break;

			case 2:
				printf("\n\t...Actualizando valores...\n");
				printf("Ingresa el nuevo valor de 1 dólar a Pesos: ");
				scanf("%lf",&valor);

				if(valor >=0){

				printf("\n\t...Valor Actualizado...");
				printf("\n1 dólar = %.3f pesos\n",valor);

				}else{

					printf("\n\t...ERROR; Ingresa un valor positivo...\n");

				}

				break;

			case 0:
				printf("\nPrograma desarrollado por: Jonathan Martiñón.\n\n");
				break;
			default:
				printf("\n\t...Error; Número fuera de rango...\n");
				break;

		}

	}

}
