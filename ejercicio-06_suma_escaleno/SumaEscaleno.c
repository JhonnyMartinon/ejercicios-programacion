#include <stdio.h>

int main(){

	float Lados[3];
	float suma;
	int Op = 1;

	while(Op!=0){

		printf("\n\nOpciones:\n");
		printf("1) Calcular nuevo perímetro.\n");
		printf("0) Salir.\n");
		printf("Opción seleccionada: ");
		scanf("%d",&Op);

		switch(Op){

			case 1:
				suma = 0.0;

				for (int i = 0; i<3; i++){
					Lados[i]=0.0;
					printf("\nIngresa lado %d: ",i+1);
					scanf("%f",&Lados[i]);
					if(Lados[i]<=0){
						printf("\n\t...ERROR; El lado debe ser mayor a 0...\n");
						break;
					}
					suma +=Lados[i];
				}

				if(Lados[0] <=0 || Lados[1]<=0 || Lados[2]<=0){
					break;
				}else{
					printf("\nEl triángulo con lados: (%.2f,%.2f,%.2f) tiene un perímetro de: %.2f\n",Lados[0],Lados[1],Lados[2], suma);
				}

				break;

			case 0:
				printf("\nPrograma desarrollado por: Jonathan Martiñón.\n\n");
				break;


			default:

				printf("\n\t...ERROR; Número fuera de rango...\n");
				break;

		}
	}

}
