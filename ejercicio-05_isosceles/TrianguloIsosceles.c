#include <stdio.h>

int main(){

	float lado = 0.0;
	float base = 0.0;
	int Op = 1;

	while(Op!=0){

		printf("\n\nOpciones:\n");
		printf("1) Calcular nuevo promedio.\n");
		printf("0) Salir.\n");
		printf("Opción seleccionada: ");

		scanf("%d",&Op);

		switch(Op){

			case 1:
				printf("\nIngresa el valor de la Base: ");
				scanf("%f",&base);

				if(base > 0){

					printf("\nIngresa el valor de un lado: ");
					scanf("%f",&lado);

					if(lado>0){
					
						if(base<lado*2){

							printf("\nEl perímetro del triángulo Isósceles con base: %.3f y lados: %.3f y %.3f",base,lado,lado);
							printf("\n es: %.3f",base + (lado*2) );

						}else{

							printf("\n\t...ERROR; La base debe ser menor que 2 veces el lado...\n");
						}


					}else{
						printf("\n\t...ERROR; Se requiere valor mayor a 0...\n");
					}
					
				}else{
					printf("\n\t...ERROR; Se requiere valor mayor a 0...\n");
				}
				break;

			case 0:
				printf("\nPrograma desarrollado por: Jonathan Martiñón.\n\n");
				break;

			default:
				printf("\n\t...ERROR; Número fuera de rango...\n");
				break;
		
		}

	}

}
