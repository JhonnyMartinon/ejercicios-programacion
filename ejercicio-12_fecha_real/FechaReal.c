#include <stdio.h>
#include <stdbool.h>

void Imprimefecha(int D, int M, int Y){
	switch (M){
		case 1:
			printf("%d / Enero / %d",D,Y);
			break;
		case 2:
			printf("%d / Febrero / %d",D,Y);
			break;
		case 3:
			printf("%d / Marzo / %d",D,Y);
			break;
		case 4:
			printf("%d / Abril / %d",D,Y);
			break;
		case 5:
			printf("%d / Mayo / %d",D,Y);
			break;
		case 6:
			printf("%d / Junio / %d",D,Y);
			break;
		case 7:
			printf("%d / Julio / %d",D,Y);
			break;
		case 8:
			printf("%d / Agosto / %d",D,Y);
			break;
		case 9:
			printf("%d / Septiembre / %d",D,Y);
			break;
		case 10:
			printf("%d / Octubre / %d",D,Y);
			break;
		case 11:
			printf("%d / Noviembre / %d",D,Y);
			break;
		case 12:
			printf("%d / Diciembre / %d",D,Y);
			break;
		default:
			printf("%d / %d / %d",D,M,Y);
			break;
	}
}

bool EsBisiesto(int Y){

	if(Y%4==0){
		if(Y%100==0){
			if(Y%400==0){
				return true;
			}else{
				return false;
			}
		}else{
			return true;
		}												
	}else{						
		return false;
	}

}

int main(){

	int date[3], Op = 1;
	bool flag;

	while(Op!=0){
		printf("\n\nOpciones:");
		printf("\n1) Verificar Fecha.");
		printf("\n0) Salir.");
		printf("\nOpción seleccionada: ");
		scanf("%d",&Op);

		switch(Op){
			case 1:

				flag = true;
				for (int i=0; i<3;i++){
					date[i] = 0;
				}

				printf("\nIngresa el año: ");
				scanf("%d",&date[0]);
				printf("\nIngresa el número de mes: ");
				scanf("%d",&date[1]);
				printf("\nIngresa el día: ");
				scanf("%d",&date[2]);

				for (int i=0;i<3;i++){
					if (date[i]<1){
						flag = false;
					}
				}

				if (flag){

					printf("\nLa fecha: ");
					Imprimefecha(date[2],date[1],date[0]);

					if (date[1]==1 || date[1]==3 || date[1]==5 || date[1]==7 || date[1]==8 || date[1]==10 ||date[1]==12){
						if(date[2]<=31){
							printf("\nEs válida.");
						}else{
							printf("\nNo es válida.");
						}
					}else{
						if(date[1]==4 || date[1]==6 || date[1]==9 || date[1]==11){
							if(date[2]<=30){
								printf("\nEs válida.");
							}else{
								printf("\nNo es válida.");
							}
						}else{
							if(date[1]==2){

								if (date[0]>=4){

									if(EsBisiesto(date[0])){
										if(date[2]<=29){
											printf("\nEs válida.\n(Fue año bisiesto).");
										}else{
											printf("\nNo es válida.");
										}
									}else{
										if(date[2]<=28){
											printf("\nEs válida.");
										}else{
											printf("\nNo es válida.");
										}
									}
								}else{
									printf("\n\t...ERROR; Ingresa un año mayor o igual a 4...\n");
								}

							}else{
								printf("\nNo es válida.");
							}
						}
					}

				}else{
					printf("\n\t...ERROR; Ingresa números mayores a 0...\n");
				}

				break;
			case 0:
				printf("\nPrograma desarrollado por: Jonathan Martiñón.\n\n");
				break;
			default:
				printf("\n\t...ERROR; Número fuera de rango...\n");
				break;
		}
	}

	
}
