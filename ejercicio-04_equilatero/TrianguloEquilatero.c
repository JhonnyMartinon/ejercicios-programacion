#include <stdio.h>

int main(){

	float lado = 0.0;
	int Op = 1;

	while(Op!=0){

		printf("\n\nOpciones:\n");
		printf("1) Calcular perímetro.\n");
		printf("0) Salir.\n");

		printf("Opción seleccionada: ");
		scanf("%d",&Op);
		
		switch(Op){
		
			case 1:

				printf("\nIngresa el valor de 1 lado del triángulo: ");
				scanf("%f",&lado);

				if(lado >0){

					printf("El triángulo equilátero con lados de medida %.2f; tiene un perímetro de: %.2f",lado,lado*3);

				}else{

					printf("\n\tERROR; Ingresa números positivos...\n\n");
				}

				break;

			case 0:
				printf("\nPrograma desarrollado por: Jonathan Martiñón.\n");
				break;

			default:

				printf("\n\t...ERROR; Número fuera de rango...\n\n ");
				break;

		}
	
	}

}
