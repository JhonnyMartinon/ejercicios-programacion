#include <stdio.h>
#include <stdbool.h>

int main(){

	int Op = 1;
	float Lados[3];
	bool F;
	float suma;

	while(Op!=0){

		for (int i = 0; i<3; i++){
			Lados[i]=0.0;
		}

		printf("\n\nPrograma para calcular perímetros.\n");
		printf("\nOpciones:\n");
		printf("1) Triángulo Equilátero.\n");
		printf("2) Triángulo Isósceles.\n");
		printf("3) Triángulo Escaleno.\n");
		printf("0) Salir.\n");
		printf("Opción Seleccionada: ");

		scanf("%d",&Op);

		switch(Op){

			case 1:
				printf("\nIngresa el valor de 1 lado del triángulo: ");
				scanf("%f",&Lados[0]);

				if(Lados[0] >0){

					printf("El triángulo equilátero con lados de medida %.2f; tiene un perímetro de: %.2f",Lados[0],Lados[0]*3);

				}else{

					printf("\n\t...ERROR; Se requiere valor mayor a 0...\n\n");
				}
				break;
			case 2:
				printf("\nIngresa el valor de la Base: ");
				scanf("%f",&Lados[0]);

				if(Lados[0] > 0){

					printf("\nIngresa el valor de un lado: ");
					scanf("%f",&Lados[1]);

					if(Lados[1]>0){

						if(Lados[0]<Lados[1]*2){

							printf("\nEl perímetro del triángulo Isósceles con base: %.3f y lados: %.3f y %.3f",Lados[0],Lados[1],Lados[1]);
							printf("\n es: %.3f",Lados[0] + (Lados[1]*2) );

						}else{

							printf("\n\t...ERROR; La base debe ser menor que 2 veces el lado...\n");
						}


					}else{
						printf("\n\t...ERROR; Se requiere valor mayor a 0...\n");
					}

				}else{
					printf("\n\t...ERROR; Se requiere valor mayor a 0...\n");
				}
				break;
			case 3:
				F = true;
				suma = 0.0;

				for (int i = 0; i<3; i++){
					printf("\nIngresa lado %d: ",i+1);
					scanf("%f",&Lados[i]);
					if(Lados[i]<=0){
						F = false;
						printf("\n\t...ERROR; Se requiere valor mayor a 0...\n");
						break;
					}
					suma +=Lados[i];
				}

				if(!F){
					break;
				}else{
					printf("\nEl triángulo con lados: (%.2f,%.2f,%.2f) tiene un perímetro de: %.2f\n",Lados[0],Lados[1],Lados[2], suma);
				}

				break;
			case 0:
				printf("\nPrograma desarrollado por: Jonathan Martiñón.\n\n");
				break;
			default:
				printf("\n\t...ERROR;Número fuera de rango...\n");
				break;

		}

	}

}
