#include <stdio.h>
#include <stdbool.h>


bool verifica(float *L1, float *L2, float *L3){

	if(*L1 < *L2+*L3){
		return true;
	}else{
		printf("\nNo se cumple el teorema. No existe el triángulo.\n");
		return false;
	}

}



int main(){

	int Op = 1;
	float Lados[3];
	bool Flag = true;

	while(Op!=0){

		for (int i = 0; i<3;i++){
			Lados[i]=0.0;
		}

		printf("\n\nOpciones:\n");
		printf("1) Verificar Teorema.\n");
		printf("0) Salir.\n");
		printf("Opción Seleccionada: ");

		scanf("%d",&Op);

		switch(Op){

			case 1:

				for(int i = 0; i<3; i++){
					printf("\nIngresa el lado %d: ",i+1);
					scanf("%f",&Lados[i]);
					if(Lados[i]<=0){
						printf("\n\t...ERROR; Ingresa números mayores a 0...\n");
						Flag = false;
						break;
					}
				}

				if(Flag){
					if(verifica(&Lados[0],&Lados[1],&Lados[2])){
						if( verifica(&Lados[1],&Lados[2],&Lados[0])){			
							if(verifica(&Lados[2],&Lados[1],&Lados[0])){
								printf("\nSe cumple el teorema. El triángulo existe.\n");
							}
						}

					}
				}
				break;

			case 0:

				printf("\nPrograma desarrollado por: Jonathan Martiñón.\n\n");
				break;

			default:
				printf("\n\t...ERROR; Número fuera de rango...\n");
				break;

		}
	}


}

