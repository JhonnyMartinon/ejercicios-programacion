#include <stdio.h>

int main(){

	int Op = 1;
	int fecha = 400;

	while(Op!=0){

		printf("\n\nOpciones:\n");
		printf("1) Verificar fecha.\n");
		printf("0) Salir.\n");
		printf("Opción seleccionada: ");
		scanf("%d",&Op);

		switch(Op){

			case 1:
				printf("\nIngresa la fecha a verificar: ");
				scanf("%d",&fecha);
				if(fecha>=4){
					if(fecha%4==0){
						if(fecha%100==0){
							if(fecha%400==0){
								printf("La fecha %d sí es bisiesta",fecha);
							}else{
								printf("La fecha %d no es bisiesta",fecha);
							}
						}else{
							printf("La fecha %d es bisiesta",fecha);
						}												
					}else{
						
						printf("La fecha %d no es bisiesta",fecha);
					}

				}else{
					printf("\nERROR; Ingresa mínimo la fecha: 4.\n");
				}
				break;

			case 0:
				printf("\nPrograma desarrollado por: Jonathan Martiñón.\n\n");
				break;

			default:
				printf("\n\t...ERROR; Número fuera de rango...\n");
				break;
		}

	}

}
